#!/bin/sh
menu="tofi"

opts="Hibernate\nQuit\nLock\nShutdown\nReboot\nSleep"
choice=$(echo "$opts" | $menu)

case $choice in
	Quit)
		riverctl exit
		;;
	Lock)
		notify-send "Failed to Lock" "Locking not implemented yet"
		exit 1
		;;
	Shutdown)
		notify-send "Shutting Down" "System shutting down in 5 seconds" \
			&& sleep 5 \
			&& riverctl exit \
			&& sleep 1 \
			&& sudo shutdown -h now
		;;
	Reboot)
		notify-send "Rebooting" "System Rebooting in 5 seconds" \
			&& sleep 5 \
			&& riverctl exit \
			&& sleep 1 \
			&& sudo shutdown -r now
		;;
	Sleep)
		sudo zzz
		;;
	Hibernate)
		sudo ZZZ
		;;
esac

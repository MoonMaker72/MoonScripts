#!/bin/sh
case $1 in
	volume*)
		value=$(pamixer --get-volume)
		tag="volume"
		image="$HOME/.local/share/icons/BeautyLine/devices/scalable/audio-headphones-symbolic.svg"
		;;
	brightness*)
		value=$(light -G \
			| cut -d'.' -f1)
		tag="brightness"
		image="$HOME/.local/share/icons/BeautyLine/devices/scalable/monitor.svg"
		;;
	battery*)
		value=$(cat /sys/class/power_supply/BAT1/capacity)
		tag="battery"
		image="$HOME/.local/share/icons/BeautyLine/devices/scalable/ac-adapter-symbolic.svg"
		;;
	date*|time*)
		value=""
		summary=$(date +%H:%M)
		full=$(date +"%a, %d %b")
		tag="date"
		image="$HOME/.local/share/icons/BeautyLine/apps/scalable/accessories-clock.svg"
		;;
	*)
		exit 1
		;;
esac

if [ "$value" ]; then
	notify-send \
		-h int:value:"$value" \
		-h string:image-path:$image \
		-h string:wired-tag:$tag \
		-h string:x-canonical-private-synchronous:$tag \
		"$value%"
else
	notify-send \
		-h string:image-path:$image \
		-h string:wired-tag:$tag \
		-h string:x-canonical-private-synchronous:$tag \
		"$summary"
fi

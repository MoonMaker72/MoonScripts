#!/bin/sh
menu=${1:-"rofi -dmenu -p Theme: -l 10 -i -no-custom"}
configfile="${HOME}/.config/alacritty/alacritty.yml"

theme=$(ls -1 ${HOME}/.local/share/term-themes | $menu)
while [ -n "$theme" ]; do
	sed -i "s=themes/.*=themes/${theme}=" $configfile
	theme=$(ls -1 ${HOME}/.local/share/term-themes | $menu)
done

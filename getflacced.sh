#!/bin/sh

# TODO DATE
# TODO IMAGE
echo "Date not implemented"

# Set Album Name and Artist Name
RAWALBUM="${PWD##*/}"
ALBUM="${RAWALBUM%\ \(*\)}"
ARTIST=$(cd .. && echo "${PWD##*/}")

for file in *.mp3
do
	# Get Title name
	TITLE="${file%.mp3}"
	# Convert to Flac
	ffmpeg -i "$file" "${TITLE}.flac"
	# Remove All tags
	metaflac --preserve-modtime \
		--remove-all-tags "${TITLE}.flac"
	# Set Artist, Album and Title tags
	metaflac --preserve-modtime \
		--set-tag=ARTIST="${ARTIST}" \
		--set-tag=ALBUM="${ALBUM}" \
		--set-tag=TITLE="${TITLE}" "${TITLE}.flac"
done

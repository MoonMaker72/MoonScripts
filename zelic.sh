#!/bin/sh
opts="New\nNew Named\nAttach\nKill\nKill All"
opt=$(echo "$opts" | sk)

case $opt in
	Attach*)
		session=$(zellij list-sessions -s | sk)
		if [ -n "$session" ]; then
			zellij attach "$session"
		fi
		;;
	Kill\ All*)
		zellij kill-all-sessions
		;;
	Kill*)
		session=$(zellij list-sessions -s | sk)
		if [ -n "$session" ]; then
			zellij kill-session "$session"
		fi
		;;
	New\ Named*)
		printf "[Enter Session Name]: " && read -r name
		[ -n "$name" ] && zellij -s "$name" || zellij -s "unnamed"
		;;
	New)
		zellij
		;;
esac

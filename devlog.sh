#!/bin/sh

## TODO: Single file with all logs
new() {
	entry=$(mktemp --suffix dlog)
	today=$(date -u +%m-%d-%y)
	log_file="$today.dlog"
	$EDITOR $entry
	if [ ! -d "./.dlogs" ]; then
		mkdir .dlogs
	fi
	cat $entry >> $log_file
}

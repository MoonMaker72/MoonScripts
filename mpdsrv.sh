#!/bin/sh

query_file=$(mktemp --suffix="mpdsrv")
pgrep -x mpd 2>/dev/null || mpd
python -m http.server --directory ~/Music 8000 >> "$query_file"

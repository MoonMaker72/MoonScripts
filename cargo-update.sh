#!/bin/sh

pkgs=$(cargo install --list | grep -Eoh '^[a-z|\-]*')
for pkg in $pkgs; do
	echo "Updating $pkg"
	cargo install "$pkg"
done

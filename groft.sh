#!/bin/sh
usage() {
	cat <<-EOF
	Usage: groft.sh <input-file> <output-file> [output-format] [input-format] [groff-options]
	EOF
}

if [ -z "$1" ]; then
	usage
	exit
elif [ -z "$2" ]; then
	usage
	exit
fi

if [ -z "$4" ]; then
	INPUT_EXT="${1##*.}"
	[ -z "$INPUT_EXT" ] && INPUT_EXT="me"
else
	INPUT_EXT="$4"
fi

if [ -z "$3" ]; then
	OUTPUT_EXT="${2##*.}"
	[ -z "$OUTPUT_EXT" ] && OUTPUT_EXT="pdf"
else
	OUTPUT_EXT="$4"
fi

groff -"$INPUT_EXT" -T "$OUTPUT_EXT" "$1" > "$2"

#!/bin/sh
RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[0;33m'
CL='\033[0m'

logstati() {
	stat="$?"
	if [ -z "$status" ]; then
		status="$1 Update"
	else
		status="$status\n$1 Update"
	fi
	if [ "$stat" -eq 0 ]; then
		status="$status: ${GREEN}Completed${CL}"
	else
		status="$status: ${RED}Failed${CL}"
	fi
}
sep() {
	printf "${YELLOW}### %s ###${CL}\n" "$1"
}

sep "XBPS System Update"
sudo xbps-install -Syu
logstati "XBPS"

sep "RUST Update"
rustup update
logstati "RUST"

sep "Cabal Update"
cabal update
logstati "Cabal"

sep "NODE Update"
npm -g update
logstati "NODE"

sep "NeoVim Update"
nvim -c PackerSync -c q
logstati "NeoVim Pkgs"

sep "Tealdeer Update"
tldr -u
logstati "Tealdeer cache"

sep "Update Complete"
echo "$status" | grep "Failed" && notify-send "Updater" "System Update Complete with failures" || notify-send "Updater" "Full System Update Complete"
printf "$status\n"

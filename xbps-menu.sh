#!/bin/sh

opts="Update Pkgs\nInstall Pkgs\nRemove Pkgs\nUpgrade Xbps\nSync Repos\nClean Orphan Pkgs\nClear Cache"
clean_pkg_name() {
	while read pkg
	do
		printf "%s" "$pkg" | cut -d' ' -f2 | sed 's/-[0-9|a-z|.]*[0-9]*_[0-9]*$//'
	done
}
choose_pkgs() {
	sk --multi | clean_pkg_name | tr "\n" " "
}
die() {
	notify-send "Xbps $1 Error" "An error occured. $2"
	exit 2
}
complete() {
	notify-send "Xbps $1 Complete" "$2"
	exit 0
}

ch=$(echo "$opts" | sk)

case $ch in
	Update*)
		sudo xbps-install -Su || die "Update" "Failed to update package(s)"
		complete "Update" "System is up to date"
		;;
	Install*)
		pkgs=$(xbps-query -Rs '' \
			| grep --invert-match "\[\*\]" \
			| choose_pkgs)
		[ -z "$pkgs" ] && echo "No pkg selected" && exit
		printf "Installing: %s\n" "$pkgs"
		sudo xbps-install -S $pkgs || die "Installation" "Failed to install package(s)"
		complete "Installation" "Package installation complete"
		;;
	Remove*)
		pkgs=$(xbps-query -s '' \
			| choose_pkgs)
		[ -z "$pkgs" ] && echo "No pkg selected" && exit
		printf "Removing: %s\n" "$pkgs"
		sudo xbps-remove -R $pkgs || die "Removal" "Failed to remove packages"
		;;
	Upgrade*)
		sudo xbps-install -Su xbps || die "Update" "Failed to update xbps"
		complete "Upgrade" "Xbps is up to date"
		;;
	Sync*)
		sudo xbps-install -S || die "Sync" "Falied to sync repos"
		;;
	Clean*)
		pkgs=$(xbps-query -O \
			| clean_pkg_name \
			| tr '\n' ' ')
		[ -z "$pkgs" ] && echo "No Orphan Packages" && exit
		printf "Removing: %s\n" "$pkgs"
		sudo xbps-remove -R $pkgs || die "Removal" "Failed to remove orphan packages"
		;;
	Clear*)
		sudo xbps-remove -O || die "Clear" "Failed to clear cache"
		;;
esac

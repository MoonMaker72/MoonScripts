#!/bin/bash
repos=(
	"mirror.ps.kz" \
	"mirrors.bfsu.edu.cn" \
	"mirrors.cnnic.cn" \
	"mirrors.tuna.tsinghua.edu.cn" \
	"mirror.sjtu.edu.cn" \
	"void.webconverger.org" \
	"mirror.aarnet.edu.au" \
	"ftp.swin.edu.au" \
	"void.cijber.net" \
	"ftp.dk.xemacs.org" \
	"mirrors.dotsrc.org" \
	"quantum-mirror.hu" \
	"voidlinux.mirror.garr.it" \
	"mirror.fit.cvut.cz" \
	"ftp.debian.ru" \
	"mirror.yandex.ru" \
	"cdimage.debian.org" \
	"ftp.acc.umu.se" \
	"ftp.lysator.liu.se" \
	"ftp.sunet.se" \
	"void.sakamoto.pl" \
	"mirror.clarkson.edu" \
	"alpha.de.repo.voidlinux.org" \
	"repo-fi.voidlinux.org" \
	"mirrors.servercentral.com" \
	"repo-us.voidlinux.org"
)

fping=1000
frepo=""

for repo in "${repos[@]}"
do
	printf "%s\n" "Pings for $repo"
	ping=$(ping -c 4 "$repo" | tail -1 | cut -d' ' -f4 | cut -d'/' -f2 | bc -l)
	printf "=> %s\n" "$repo avg ping: $ping"
	if (( $(bc <<< "$ping<$fping") ))
	then
		frepo=$repo
		fping=$ping
	fi
done

printf "%s\n" "Reccomended repo is: $frepo"
printf "%s\n" "Ping: $fping"

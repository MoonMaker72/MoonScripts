#!/bin/sh

# Set Up
main_site="https://xoxocomics.net"
download_dir="${HOME}/Documents/Comics"
options="search\nhot\nnew-comics\npopular-comics"
menu="sk"

# DRY
pad_number()
{
	x=$1
	while [ ${#x} -ne 5 ];
	do
	        x="0"$x
	done
	echo "$x"
}
trim()
{
	trim=${1#${1%%[![:space:]]*}}
	trim=${trim%${trim##*[![:space:]]}}
	printf "%s" "$trim"
}

# What do you want?
if [ -n "$1" ]; then
	keyword=$(echo "$1" | sed 's/ /+/g')
	choice="search?keyword=$keyword"
else
	choice=$(echo "$options" | $menu --header="What do you want?")
fi
if [ -z "$choice" ]; then
	echo "Nothing Selected"
	exit
elif [ "$choice" = "search" ]; then
	printf "Search Comic: " && read -r keyword
	keyword=$(echo "$keyword" | sed 's/ /+/g')
	choice="search?keyword=$keyword"
fi

# Choose Comic
echo "$main_site/$choice"
comic=$(curl -s "$main_site/$choice" \
	| grep '<a title=".*" href="https://xoxocomics.net/comic/.*">' \
	| sed 's|<a title=".*" href="https://xoxocomics.net/comic/||' \
	| sort -u \
	| sed 's|">||' \
	| $menu --header="Choose Comic" \
)
if [ -z "$comic" ]; then
	echo "No valid comic Chosen"
	exit
fi
comic=$(trim "$comic")

# Get metadata
main_site="$main_site/comic/$comic"
issues=$(curl -s "$main_site" \
	| grep -Eoh "issue-.*/[0-9]+" \
	| sort -u --field-separator=- --key=2 \
)

# Choose issue
issues=$(printf "%s" "$issues" | $menu -m --header="Choose Issue(s)")
if [ -z "$issues" ]; then
	echo "No valid issue Chosen"
	exit
fi

# Check download_dir exists
if [ -d "$download_dir" ]; then
	echo "Download Dir: Exists"
fi

# Make dir for the comic and issue
printf "%s\n" "$issues" | while IFS= read -r issue
do
	issue_number=$(pad_number "$(printf "$issue" | sed -e "s/^issue-//" -e "s/\/[0-9]*$//")")
	final_dir="${download_dir}/${comic}/issue-${issue_number}"
	if [ -d "$final_dir" ]; then
		cd "$final_dir"
	else
		mkdir -p "$final_dir"
		cd "$final_dir"
	fi
	
	# Download issue images
	echo "Downloading Pages into $final_dir"
	pages=$(curl -s "$main_site/$issue" \
		| grep -Eoh "$main_site/$issue/[0-9]+" \
		| sort -u --field-separator=/ --key=8n \
		| sed -e "0,/1/ s|$main_site/$issue/1|$main_site/$issue/0|"
	)
	pages="$pages"
	echo "Pages $pages"
	printf "%s" "$pages" | while IFS= read -r line
	do
		page_number=$(pad_number "$(echo "$line" | cut -d'/' -f8)")
		echo "$page_number"
		echo "$page"
		page=$(curl -s "$line" | grep -Eoh -m 1 "https://2.bp.blogspot.com/([0-9A-Za-z]|-|=|_)*")
		#printf "%s\n" "Downloading page $page_number of issue $issue"
		wget -O "${page_number}.jpg" "$page"
	done
done

#!/bin/sh

if [ -n "$1" ]; then
	brightness_now=$(light -G \
		| cut -d '.' -f1)
	case "$1" in
		-inc)
			#brightness_new=$(expr $brightness_now + ${2:-5})
			light -A ${2:-5}
			;;
		-dec)
			#brightness_new=$(expr $brightness_now - ${2:-5})
			light -U ${2:-5}
			;;
		*)
			exit 1
			;;
	esac
	$HOME/scripts/util_notif.sh brightness
else
	exit 1
fi

#xbacklight -set $brightness_new

#!/bin/sh
usage()
{
	cat <<-EOF
	Usage: blk.sh [COMMAND]
	Execute commands on multiples/all files in cwd

	  COMMAND: command to prefix directory items with

	Examples:
	  blk.sh rm
	EOF
}

if [ "$1" = "--help" ]; then
	usage
	exit
elif [ "$1" = "-h" ]; then
	usage
	exit
fi

temp_file=$(mktemp --suffix="blk")

if [ -z "$1" ] || [ "$1" = "mv" ]; then
	lsd -1 \
		| xargs -d '\n' -I'{}' \
			printf "# mv|\"%s\"|\"%s\"\n" "{}" "{}" \
		| column -t -s '|' >> "$temp_file"
else
	lsd -1 \
		| xargs -d '\n' -I'{}' \
			printf "# $1|\"%s\"\n" "{}" \
		| column -t -s '|' >> "$temp_file"
fi

$EDITOR "$temp_file"
sh "$temp_file"

rm "$temp_file"

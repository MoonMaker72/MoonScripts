#!/bin/sh
preview_cmd='mpc -q playlist'
menu="sk --layout=reverse-list --margin 3 --preview-window=up --print0"
playlist_dir="$HOME/Music/playlists"

choose_song() {
	mpc -q listall | $menu --header="${1:-Select Song(s)}" --multi
}
choose_from_queue() {
	mpc -q playlist | awk -v ln=1 '{print ln++ ":" $0}' | $menu --header="${1:-Select Song from queue}"
}
choose_playlist() {
	mpc -q lsplaylists | $menu --header="${1:-Select Playlist}"
}
confirm_choice() { printf "Yes\nNo" | $menu --header="${1:-Are You Sure?}"; }
notif() { notify-send "Queuec" "$@"; }
opts() {
	cat <<-EOF
	 1: Play/Pause Queue
	 2: Add Song to Queue
	 3: Insert Song to Next in Queue
	 4: Clear Current Queue
	 5: Crop Current Queue
	 6: Move Song in Queue
	 7: Shuffle Current Queue
	 8: Remove Song From Queue
	 9: Play Song From Queue
	10: Skip to Next/Prev Song in Queue
	11: Save Current Song to Playlist
	12: Load Playlist to Queue
	13: Save Queue as Playlist
	EOF
}

ch=$(opts | $menu --preview="$preview_cmd")

while [ -n "$ch" ]; do

	case $ch in
		\ 1:*)
			mpc -q toggle
			;;
		\ 2:*)
			choose_song "Select Song to be Added" | xargs -0 -I "{}" mpc -q add "{}"
			;;
		\ 3:*)
			choose_song "Select Song to be Inserted" | xargs -0 -I '{}'  mpc -q insert "{}"
			;;
		\ 4:*)
			sure="Are you sure you want to CLEAR the current queue?"
			[ "$(confirm_choice "$sure")" = "Yes" ] && mpc -q clear
			;;
		\ 5:*)
			sure="Are you sure you want to CROP the current queue?"
			[ "$(confirm_choice "$sure")" = "Yes" ] && mpc -q crop
			;;
		\ 6:*)
			pos_from=$(choose_from_queue "Select Song to be moved" | cut -z -d':' -f1)
			while [ -n "$pos_from" ]; do
				pos_to=$(choose_from_queue "Select Moved Location" | cut -z -d':' -f1)
				if [ -n "$pos_to" ]; then
					mpc -q move "$pos_from" "$pos_to"
					pos_from=$(choose_from_queue "Select Song to be moved" | cut -z -d':' -f1)
				fi
			done
			;;
		\ 7:*)
			sure="Are you sure you want to SHUFFLE the current queue?"
			[ "$(confirm_choice)" = "Yes" ] && mpc -q shuffle
			;;
		\ 8:*)
			pos=$(choose_from_queue "Choose Song to be removed" | cut -z -d':' -f1)
			while [ -n "$pos" ]; do
				mpc -q del "$pos"
				pos=$(choose_from_queue "Choose Song to be removed" | cut -z -d':' -f1)
			done
			;;
		\ 9:*)
			choose_from_queue "Choose Song to Start Playing" \
				| cut -d':' -f1 \
				| xargs -0 -I '{}' mpc -q play '{}'
			;;
		10:*)
			printf "next - Start Playing Next Song\nprev - Start Playing the Previous Song" \
				| $menu \
				| cut -z -d' ' -f1 \
				| xargs -0 -I '{}' mpc -q '{}'
			;;
		11:*)
			playlist=$(choose_playlist "Choose playlist to save Song to")
			[ -n "$playlist" ] && mpc -q --format '%file%' current >> "$playlist_dir/$playlist.m3u" && notif "Added Song To $playlist"
			;;
		12:*)
			playlist=$(choose_playlist "Choose Playlist to Load")
			[ -n "$playlist" ] && mpc -q load "$playlist"
			;;
		13:*)
			clear
			printf "[Enter Playlist Name]: " && read -r name
			if [ -n "$name" ]; then
				[ -f "$playlist_dir/$name.m3u" ] && rm "$playlist_dir/$name.m3u"
				mpc -q save "$name" && notif "Saved Queue as $name"
			fi
			;;
	esac

	ch=$(opts | $menu --preview="$preview_cmd")

done

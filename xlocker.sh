#!/bin/sh
#Confirm if another lock isn't up
pgrep -x i3lock >/dev/null && exit

# Setting up colors
CLEAR=00000000
RED=f85e84ff
MAGENTA=ab9df2ff
BLUE=7accd7ff
YELLOW=e5c463ff

# Take Screenshot, Blur Screenshot, Use Screenshot as bg
maim > /tmp/xlocked.png && \
	convert /tmp/xlocked.png \
	-filter Gaussian \
	-blur 0x8 \
	/tmp/xlocked.png && \
	i3lock -i /tmp/xlocked.png \
	-e -F \
	--force-clock \
	--inside-color=$CLEAR \
	--insidever-color=$CLEAR \
	--insidewrong-color=$CLEAR \
	--ring-color=$CLEAR \
	--ringver-color=$BLUE \
	--ringwrong-color=$YELLOW \
	--verif-color=$CLEAR \
	--wrong-color=$CLEAR \
	--modif-color=$CLEAR \
	--line-color=$CLEAR \
	--separator-color=$RED \
	--keyhl-color=$RED \
	--bshl-color=$MAGENTA \
	--time-color=$MAGENTA \
	--date-color=$MAGENTA \
	--greeter-color=$MAGENTA \
	--time-align=1 \
	--date-align=1 \
	--greeter-align=2 \
	--greeter-text="$USER" \
	--time-str="%H:%M" \
	--date-str="%A, %d %B" \
	--time-font="Oswald ExtraLight" \
	--date-font="Oswald SemiBold" \
	--greeter-font="Oswald ExtraLight" \
	--layout-font="Oswald SemiBold" \
	--time-size=48 \
	--date-size=24 \
	--greeter-size=48 \
	--time-pos="80:690" \
	--greeter-pos="1286:710" \
	--radius=80 \
	--ring-width=7

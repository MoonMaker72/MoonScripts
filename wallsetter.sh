#!/bin/sh

rootdir="$HOME/Pictures/wallpapers"
directories=$(ls -1 $rootdir)
menu='rofi -i -dmenu'

# Walrapture
walrapture()
{
	if [ -n "$1" ]; then
		notify-send -c "Alerting" "Rapturing" "Downloading for $1"
		for num in $(seq 5); do
			wget -nc $(curl --get "https://wallhaven.cc/api/v1/search?q=$1&sorting=$2&atleast=3840x2160&order=asc&page=$num" \
				| jq \
				| grep -Eoh "https:\/\/w\.wallhaven.cc\/full\/.*(jpg|png)\b")
		done
		notify-send -c "Alerting" "Raptured" "Downloaded for $1"
	fi
}
# Get the action
get_choice()
{
	choice=$(printf "Browse\nDownload\nStatus\nCancel" | $menu -p ">>")
}
# Get category
get_category()
{
	category=$(printf "hot\ntoplist\nrandom" | $menu -p "Category:")
}
# Get the walls
get_walls()
{
	if [ -n "$1" ]; then
		download_dir="$rootdir/$1"
		if [ ! -d "$download_dir" ]; then
			mkdir -p "$download_dir"
		fi
		cd $download_dir
		walrapture "${PWD##*/}" ${2:-"toplist"}
	fi
}
# Show the walls
show_walls()
{
	if [ -n "$1" ]; then
		browse_dir="$rootdir/$1"
		nsxiv -f $browse_dir
	fi
}

if [ "$1" = "--menu" ]; then
	get_choice $choice

	# If wanna get new walls
	if [ $choice = "Download" ]; then
		# Ask if already existing walls or new dir
		dir=$(printf "NewDirectory\n%s" "$directories" | $menu -p "Download:")

		# Check if choice made
		if [ -n "$dir" ]; then

			# If new dir requested
			# then ask for name and create if needed
			if [ $dir = "NewDirectory" ]; then
				dir=$($menu -p "Name:")

				# Exit if not name given
				if [ -z "$dir" ]; then
					exit
				fi
			fi

			# Get Down category
			get_category $category
			get_walls $dir ${category:-"toplist"}
		fi

	# Show available dirs for browsing
	elif [ $choice = "Browse" ]; then
		dir=$(printf "%s" "$directories" | $menu -p "Browse:")
		if [ -d "$rootdir/$dir" ]; then
			show_walls $dir
		fi

	elif [ $choice = "Status" ]; then
		status_message="Work in Progress" #pgrep wallsetter | wc -l | xargs -I '{}' echo "Downloading: {}"
		notify-send "Wall Status" "$status_message"

	# If cancelled then exit
	elif [ $choice = "Cancel" ]; then
		exit
	fi
elif [ "$1" = "--down" ]; then
	if [ -n "$2" ]; then
		[ -z "$3" ] && printf "filter not provided, defaulting to toplist"
		get_walls $2 ${3:-"toplist"}
	else
		printf "search term not provided\n"
		exit 1
	fi
fi

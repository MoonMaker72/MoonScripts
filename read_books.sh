#!/bin/sh

rootdir="$HOME/Documents/Books/"
find "$rootdir" -type f | \
	cut --delimiter="/" -f 6- | \
	tofi --width=100% --height=32% --anchor=top | \
	xargs -I'[]' zathura "$rootdir/[]" &

#!/bin/sh

SITE="https://plato.stanford.edu"
ARTICLE_PATH="$HOME/Documents/PlatoEduArticles"

fetchArticle() {
	URL=$1
	curl -s "$URL" \
		| sed -n '/<!-- BEGIN ARTICLE/,/<!-- END ARTICLE/ p' \
		| html2text
}
fetchSavedArticles() {
	find $ARTICLE_PATH -type f -name "*.${1-*}" \
		| grep "$2" \
		| xargs -I "{}" basename "{}" \
		| cut -d '.' -f 1
}
searchArticles() {
	curl -s "$SITE/search/searcher.py?query=$1" \
		| grep -Eoh "$SITE/entries/[a-z|\-]*/"
}
readSavedArticle() {
	fetchSavedArticles "pdf" "$1" \
		| sk \
		| xargs -I "{}" zathura "$ARTICLE_PATH/{}.pdf"
}
convertSavedArticle() {
	article=$(fetchSavedArticles "md" "$1" \
		| sk)
	if [ -n "$article" ]; then
		printf "[Name](leave blank for original name): " && read -r name
		if [ -z "$name" ]; then
			name=$article
		fi
		pandoc -t pdf \
			--pdf-engine=wkhtmltopdf \
			"$ARTICLE_PATH/$article.md" -t "${2:-pdf}" -o "$ARTICLE_PATH/$name.${2:-pdf}"
	fi
}

if [ -z "$1" ]; then
	echo "Flags: -r, -c, -f, -s"
	exit 1
fi

case "$1" in
	-r|--read)
		readSavedArticle "$2"
		;;
	-c|--convert)
		convertSavedArticle "$2" "$3"
		;;
	-f|--fetch)
		if [ -z "$2" ]; then
			echo "No url provided"
			echo "Use -s to search instead"
			exit
		fi
		fetchArticle "$2"
		;;
	-s|--search)
		printf "[Search]: " && read -r search
		echo "Searching for: $search"
		entry=$(searchArticles "$search" \
			| sk)
		
		# Check If Entry Exists
		if [ -z "$entry" ]; then
			exit 1
		fi
		
		# Save The Entry
		filename=$(echo "$entry" | sed -e "s_$SITE/entries/__" -e "s_/\$__")
		fetchArticle "$entry" > $ARTICLE_PATH/$filename.md
		;;
esac

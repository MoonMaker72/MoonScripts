#!/bin/sh

WATCH_DIR="$HOME/Videos"
OPTS="Movies\nShows\nAnime"

RUNNER="tofi"
PLAYER="mpv"

ch=$(printf "$OPTS" | $RUNNER)

if [ -n "$ch" ]; then
	case "$ch" in
			Movies) play_dir="$WATCH_DIR/Movies"
			find -L "$play_dir" -name '*.mp4' -o -name '*.mkv' -print0 \
				| cut -z -d'/' -f 6- \
				| tr "\0" "\n" \
				| $RUNNER \
				| xargs -I '{}' $PLAYER "$play_dir/{}"
			;;
		Shows|Anime) show_dir=$(find "$WATCH_DIR/$ch" -mindepth 1 -maxdepth 1 -print0 \
			| cut -z -d'/' -f 6- \
			| tr "\0" "\n" \
			| $RUNNER)
					play_dir="$WATCH_DIR/$ch/$show_dir"
					if [ ! -z $play_dir ]; then
						find -L "$play_dir" -name '*.mp4' -o -name '*.mkv' -print0 \
							| cut -z -d'/' -f 6- \
							| tr "\0" "\n" \
							| $RUNNER \
							| xargs -I '{}' $PLAYER "$play_dir/{}"
					fi
			;;
			*) printf "Unrecognized Directory"
			;;
		esac
	fi

#find -L $watch_dir -maxdepth 1 | cut -d '/' -f 5-


#!/bin/sh
usage() {
	cat <<-EOF
	Usage: fwatch.sh <FILE> <COMMAND>
	Watch a file and execute arbitrary commands on file modification

	 FILE: Path to file that is to be watched
	 COMMAND: Command to be executed on file changes

	Example:
	 fwatch.sh "myfile" "echo 'file modified'"
	EOF
}

if [ -z "$1" ]; then
	usage
	exit
elif [ -z "$2" ]; then
	usage
	exit
fi

if [ -e "$1" ]; then
	while true; do
		new=$(ls --full-time "$1" | cut -d' ' -f7)

		if [ "$new" != "$old" ]; then
			old=$new
			sh -c "$2" 2> /dev/null
		fi

		sleep 0.5
	done
else
	echo "$1 doesnt exist"
	exit
fi

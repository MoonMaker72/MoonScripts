#!/bin/sh

die() {
	echo "[E]: $*"
	exit 1
}

if [ -z "$1" ]; then
	die "Session name required"
fi

cat > ${HOME}/.xinitrc <<-EOF
#!/bin/sh

if test -z "\$DBUS_SESSION_BUS_ADDRESS"; then
	eval \`dbus-launch --sh-syntax\`
	echo "D-Bus address: \$DBUS_SESSION_BUS_ADDRESS"
fi

pipewire &
xrdb -merge \$HOME/.Xresources
xsetroot -cursor_name left_ptr
xset r rate 300 50
xrandr --dpi 192
EOF

case "$1" in
	bspwm)
		cat >> ${HOME}/.xinitrc <<-EOF
		wired &
		xwallpaper --maximize ~/Pictures/.xwallpaper
		redshift &
		#pgrep -x batterd || batterd &

		exec bspwm
		EOF
		;;
	plasma|kde)
		cat >> ${HOME}/.xinitrc <<-EOF
		exec startplasma-x11
		EOF
		;;
	*)
		die "Session name not recognized"
		;;
esac

startx

#!/bin/sh

## Setup
# Here's how you get the settings
settings=$(lpoptions -l)
debug="true"
grepsets() {
	echo "$settings" \
		| grep "$1" \
		| cut -c "$2-" \
		| tr " " "\n"
}
printable() {
	for i in "$@"
	do
		if [ -z "${mimetype##*"$i"*}" ]; then
			echo "Printable"
			break
		fi
	done
}
debug() {
	if [ "$debug" = "true" ]; then
		echo [DEBUG]: "$@"
	fi
}
RED='\033[0;31m'
BLUE='\033[0;34m'
CYAN='\033[0;36m'
GREEN='\033[0;32m'
CLEAR='\033[0m'

# Here you actually get the options
pagesizes=$(grepsets PageSize 22)
colormodel=$(grepsets ColorModel 25)
outquality=$(grepsets OutputMode 27)

# Capturing the Selected Opts
c_clm=$(echo "$colormodel" | grep "\*" | cut -c 2-)
c_pgs=$(echo "$pagesizes" | grep "\*" | cut -c 2-)
c_otq=$(echo "$outquality" | grep "\*" | cut -c 2-)

# Creating The Preview Header
pheader="ColorModel: $RED$c_clm$CLEAR
PageSize: $RED$c_pgs$CLEAR
OutputQuality: $RED$c_otq$CLEAR
Selcted File: $RED${selected:-None}$CLEAR"

# Printer Options
opts="Print Selected File
Choose File
Choose ColorMode
Choose PageSize
Choose OutputQuality"

# Menu Settings
menu="sk --margin=4,15 --preview-window=up:30%:wrap"

## Start Here
# Choose and Act accordinly
opt=$(printf '%s' "$opts" | $menu --preview="echo -ne '$pheader'")

while [ -n "$opt" ]; do

	case $opt in
		Choose\ File)
			file=$(fd --type=file --color=always | $menu --ansi --preview="echo -ne 'Print: $CYAN{}$CLEAR'")
			if [ -e "$file" ]; then
				mimetype=$(file --mime-type "$file")
				printable=$(printable "image/png" "image/jpg" "application/pdf")
				debug "$printable"
				debug "$mimetype"
				if [ "$printable" = "Printable" ]; then
					notify-send "Guten" "Printing: ${file##*/}"
					selected=$file
				else
					notify-send "Guten" "Can't Print: ${file##*/}"
				fi
			fi
			;;
		Choose\ ColorMode)
			n_clm=$(echo "$colormodel" | $menu)
			[ -n "$n_clm" ] && c_clm=$n_clm
			;;
		Choose\ PageSize)
			n_pgs=$(echo "$pagesizes" | $menu)
			[ -n "$n_pgs" ] && c_pgs=$n_pgs
			;;
		Choose\ OutputQuality)
			n_otq=$(echo "$outquality" | $menu)
			[ -n "$n_otq" ] && c_otq=$n_otq
			;;
		Print)
			;;
	esac

	# Creating The Preview Header
	pheader="PageSize: $c_pgs
ColorModel: $c_clm
OutputQuality: $c_otq
Selcted File: ${selected:-None}"
	opt=$(printf '%s' "$opts" | $menu --preview="printf '%s' '$pheader'")

done

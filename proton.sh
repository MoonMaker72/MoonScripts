#!/bin/sh

# The Proton Setup is pretty broken
# and needs fixing
# Simple workaround till it's fixed
WINEPREFIX="$HOME/.local/share/proton"
wine="$HOME/Repos/Proton-7.0rc6-GE-1/files/bin/wine"
WINESERVER="$HOME/Repos/Proton-7.0rc6-GE-1/files/bin/wineserver"
WINELOADER="$HOME/Repos/Proton-7.0rc6-GE-1/files/bin/wine"

$wine $@

#!/bin/bash

awk 'gsub(/^\t.*#/, "# ") && last {print last, "\t", $0} /^[a-z|A-Z]/{last=$0}' $HOME/.config/bspwm/sxhkdrc \
	| column -t -s $'\t' \
	| rofi -i -dmenu -p "Binding:" -no-show-icons -i -theme-str '#window { width: 750; } #listview { lines: 12; }'

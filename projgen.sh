#!/bin/sh

PKG_NAME=$1
LANGUAGE=${2:-cargo}
PKG_TYPE=${3:-bin}
LICENSE=$4

AUTHOR=$(git config --global --get user.name)
EMAIL=$(git config --global --get user.email)

die() {
	echo "[E]: $*"
	exit 1
}

warn() {
	echo "[W]: $*"
}

help() {
	echo "projgen [Name] <Language> <Type>
	Languages - rust/cargo, python/py, haskell/cabal
	Type - bin, lib"
}

newcargo() {
	cargo new "$PKG_NAME" && cd "$PKG_NAME" \
		|| die "Failed to enter pkg dir"
	licensor ${LICENSE:-"GPL-2.0"} > LICENSE
}

newpython() {
	mkdir "$PKG_NAME" && cd "$PKG_NAME" \
		|| die "Failed to enter pkg dir"
	mkdir "$PKG_NAME"
	echo "print('Hello Python')" > "$PKG_NAME/__main__.py"
	touch requirements.txt
	licensor ${LICENSE:-"GPL-2.0"} > LICENSE
	git init
}

newcabal() {
	mkdir "$PKG_NAME" && cd "$PKG_NAME" \
		|| die "Failed to enter pkg dir"
	case "$PKG_TYPE" in
		lib)
			BUILD_TYPE="lib"
			;;
		libandexe)
			BUILD_TYPE="libandexe"
			;;
		bin|exe|*)
			BUILD_TYPE="exe"
			;;
	esac
	cabal init \
		--source-dir="src" \
		--application-dir="app" \
		--package-name="$PKG_NAME" \
		--author="$AUTHOR" \
		--email="$EMAIL" \
		--"$BUILD_TYPE" \
		--minimal \
		--non-interactive \
		--verbose
	licensor ${LICENSE:-"GPL-2.0"} > LICENSE
	git init
}

if [ -z "$PKG_NAME" ]; then
	die "name is required"
fi

if [ "$PKG_NAME" = "--help" ] || [ "$PKG_NAME" = "-h" ]; then
	help
	exit
fi

if [ -d "$PKG_NAME" ]; then
	die "Directory with name '$PKG_NAME' already exists"
fi

case "$LANGUAGE" in
	rust|cargo)
		echo "rust/cargo"
		newcargo
		echo "project created at $1"
		;;
	python|py)
		echo "python"
		newpython
		echo "project created at $1"
		;;
	haskell|cabal)
		echo "haskell/cabal"
		newcabal
		echo "project created at $1"
		;;
	*)
		die "Unrecognized Project language"
		;;
esac
